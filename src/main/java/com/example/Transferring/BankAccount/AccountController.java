package com.example.Transferring.BankAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/account")
public class AccountController {
    @Autowired
    private AccountService accountService;

//    @GetMapping
//    public List<Account> getAllAccounts() {
//        return accountService.getAllAccount();
//    }
//
//    @GetMapping("/{id}")
//    public Account getAccountById(@PathVariable Long id) {
//        return accountService.getByAccountId(id);
//    }
//
//    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseStatus(HttpStatus.CREATED) // Optional, sets the response status code to 201 (Created)
//    public Account createAccount(@RequestBody Account newAccount){
//        return accountService.createAccount(newAccount);
//    }

//
//    @GetMapping("/createAccountForm")
//    public String showCreateAccountForm(Model model) {
//        // Return the form for the user to input account_name and currency
//        return "createAccountForm";
//    }

    @PostMapping("/createAccount")
    public String createAccount(@RequestParam("accountName") String accountName,
                                @RequestParam("currency") String currency) {
        // Retrieve logged-in user's ID
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = Long.parseLong(authentication.getName()); // Assuming the username is the user ID

        // Create a new account with user input for account_name and currency,
        // and default values for balance and user_id
        Account newAccount = new Account();
        newAccount.setAccount_name(accountName);
        newAccount.setCurrency(currency);
        newAccount.setBalance(0); // Set balance to default value
        newAccount.setUser_id(userId); // Set user_id from the logged-in user

        // Save the account
        accountService.createAccount(newAccount);

        // Redirect to a success page or return a response accordingly
        return "redirect:/users";
    }
}
