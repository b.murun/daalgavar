package com.example.Transferring.BankAccount;

import com.example.Transferring.User;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="account")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="account_name")
    private String account_name;

    @Column(name="currency")
    private String currency;

    @Column(name="balance")
    private double balance=0;

    @Column(name="user_id")
    private long user_id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false,updatable = false)
    private User user;


}
