package com.example.Transferring.BankAccount;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountService {
    @Autowired
    AccountRepository accountRepository;

    public List<Account> getAllAccount() {
        return accountRepository.findAll();
    }
    public Account getByAccountId(Long id){
        return accountRepository.findById(id).orElse(null);
    }
//    public Account createByUserId(Account newAcount){
//        return accountRepository.save(newAcount);
//    }
public Account createAccount(Account account) {
    return accountRepository.save(account);
}
}
