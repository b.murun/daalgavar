package com.example.Transferring;


//import com.example.Transferring.BankAccount.Account;
//import com.example.Transferring.BankAccount.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import java.math.BigDecimal;
import java.util.List;

@Controller
public class AppController {
    @Autowired
    private UserRepository repo;
    @GetMapping("")
    public String viewHomePage(){
        return "index";
    }
    @GetMapping("/register")
    public String showSignUpForm(Model model){
        model.addAttribute("user", new User());
        return "signup_form";
    }
    @PostMapping("/process_register")
    public String processRegistration(User user){
        BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        String encodedPassword=encoder.encode(user.getPassword());
        user.setPassword(encodedPassword);
        repo.save(user);
        return "register_success";
    }
//    @Autowired
//    private AccountRepository accountRepository;

    @GetMapping("/user_info")
    public String getUsers(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();

        User user = repo.findByEmail(username);

        model.addAttribute("user", user);
//        List<Account> getUserAccounts=accountRepository.getUserAccountsById(user.getId());
//        BigDecimal totalBalance=accountRepository.getTotalBalance(user.getId());
//        model.addAttribute("getUserAccounts", getUserAccounts);
//        model.addAttribute("totalBalance", totalBalance);
        return "users";
    }

}
