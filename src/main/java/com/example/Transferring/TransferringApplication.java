package com.example.Transferring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransferringApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransferringApplication.class, args);
	}

}
